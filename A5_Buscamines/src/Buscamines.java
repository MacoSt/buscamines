import java.util.Scanner;

public class Buscamines {
    static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {

        joc();

    }
//comentari fet des de la web del Git
    static void joc() {

        int opc = 0;

        String[][] camp = new String[0][0];
        String[][] campMines = new String[0][0];
        int mines = 0;

        boolean inici = false;
        boolean partida = false;

        do {

            presentaMenu();

            try {
                opc = demanaCaracter();
                switch (opc) {
                case 0:
                    System.out.println("\nFi del programa.\nAdeu.");
                    break;
                case 1:

                    mostrarAjuda();

                    break;
                case 2:

                    camp = opcions();
                    mines = quantMines(camp, mines);
                    campMines = inicialitzarMines(camp, mines);

                    inici = true;

                    break;
                case 3:

                    if (inici) {
                        jugaPartida(camp, campMines, mines);
                        partida = true;

                    } else {
                        System.out.print(
                                "\nPrimer tens que omplir les especificacions del joc a l'apartat \"Opcions\"\n.");
                    }

                    break;
                case 4:

                    if (partida) {
                        veureRankings();

                    } else {
                        System.out.print("\nPer poder veure els Rankings ten que jugar una partida com a mínim.\n");
                    }

                    break;
                default:
                    System.out.print("Opció incorrecta, escull una opció entre 0 i 4: ");
                    break;
                }
            } catch (Exception e) {
                System.out.print("Només es permet introduir números entre el 0 i el 4: " + e);
                reader.nextLine();
            }
        } while (opc != 0);

    }

    static void presentaMenu() {

        System.out.print("\n\tMENÚ\n");
        System.out.print("1. Mostrar Ajuda.\n");
        System.out.print("2. Opcions.\n");
        System.out.print("3. Jugar Partida.\n");
        System.out.print("4. Veure Rangkings.\n");
        System.out.print("0. Sortir.\n");

    }

    static int demanaCaracter() {

        System.out.print("\nIntrodueix una opció: ");
        int caracterIntroduit = reader.nextInt();

        return caracterIntroduit;
    }

    static void mostrarAjuda() {

        System.out.print(
                "\nEl joc del buscamnes consisteix en aconseguir buidar totes les caselles del camp sense trobar cap mina.\n\nEn el moment que en al camp només hi hagi mines i la resta de caselles estigun buides, has guanyat i si trobes una mina has perdut.\n");

    }

    static String[][] opcions() {

        nomJugador();

        System.out.print("\nDimensions del camp. (Valors entre 5 i 15)\n");
        System.out.print("Introdueix les files: ");
        int files = dimensionsCamp();
        System.out.print("Introdueix les columnes: ");
        int cols = dimensionsCamp();

        String[][] camp = new String[files][cols];

        for (int i = 0; i < files; i++) {
            for (int j = 0; j < cols; j++) {
                camp[i][j] = "■";
            }
        }

        return camp;

    }

    static void nomJugador() {

        System.out.print("\nIntrodueix el teu nom: ");
        reader.next();

    }

    static int dimensionsCamp() {

        int num = reader.nextInt();

        while (num < 5 || num > 15) {
            System.out.print("Valor incorrecte, torna a provar: ");
            num = reader.nextInt();
        }

        return num;

    }

    static int quantMines(String[][] camp, int mines) {

        System.out.print("\nIntrodueix la quantitat de mines que vols al camp: ");
        mines = reader.nextInt();

        while (mines < 0 | mines > camp.length * camp[0].length) {
            System.out.print("Valor incorrecte, torna a provar: ");
            mines = reader.nextInt();
        }

        return mines;

    }

    static String[][] inicialitzarMines(String[][] camp, int mines) {

        String[][] campMines = new String[camp.length][camp[0].length];

        for (int i = 0; i < campMines.length; i++) {
            for (int j = 0; j < campMines[0].length; j++) {
                campMines[i][j] = "■";
            }
        }

        int p = 0;
        int maxf = campMines.length - 1;
        int minf = 0;
        int rangef = maxf - minf + 1;
        int maxc = campMines[0].length - 1;
        int minc = 0;
        int rangec = maxc - minc + 1;

        while (p < mines) {
            int f = (int) (Math.random() * rangef) + minf;
            int c = (int) (Math.random() * rangec) + minc;
            if (campMines[f][c] == "■") {
                campMines[f][c] = "✶";
                p++;
            }
        }

        return campMines;

    }

    static void jugaPartida(String[][] camp, String[][] campMines, int mines) {

        boolean partidaencurs = true;

        do {

            mostrarCamp(camp);

            System.out.print("Posa la fila: ");
            int f = reader.nextInt();
    
            System.out.print("Posa la columna: ");
            int c = reader.nextInt();
    
            f -= 1;
            c -= 1;
    
            int cont = 0;

            if (partidaencurs) {

                partidaencurs = contaMines(camp, campMines, f, c, cont, partidaencurs, mines);

                int casellescobertes = 0;

                for (int i = 0; i < campMines.length; i++) {
                    for (int j = 0; j < campMines[0].length; j++) {
                        if (camp[i][j] == "■") {
                            casellescobertes++;
                        }
                    }
                }
    
                if (casellescobertes == mines) {
                    partidaencurs = false;
                }

            }

        } while (partidaencurs);

        mostrarCampMines(campMines);

        System.out.print("Enhorabona, has guanyat!!\n");

    }

    static void mostrarCamp(String[][] camp) {

        System.out.print("\n     ");

        int posy = 0, posx = 0;

        for (int i = 0; i < camp[0].length; i++) {
            posy++;
            if (Integer.toString(posy).length() == 1) {
                System.out.print(posy + "  ");
            } else {
                System.out.print(posy + " ");
            }
        }

        System.out.print("\n\n");
        for (int i = 0; i < camp.length; i++) {
            posx++;
            if (Integer.toString(posx).length() == 1) {
                System.out.print(posx + "    ");
            } else {
                System.out.print(posx + "   ");
            }
            for (int j = 0; j < camp[0].length; j++) {
                System.out.print(camp[i][j] + "  ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

    }

    static void mostrarCampMines(String[][] campMines) {

        System.out.print("\n     ");

        int posy = 0, posx = 0;

        for (int i = 0; i < campMines[0].length; i++) {
            posy++;
            if (Integer.toString(posy).length() == 1) {
                System.out.print(posy + "  ");
            } else {
                System.out.print(posy + " ");
            }
        }

        System.out.print("\n\n");
        for (int i = 0; i < campMines.length; i++) {
            posx++;
            if (Integer.toString(posx).length() == 1) {
                System.out.print(posx + "    ");
            } else {
                System.out.print(posx + "   ");
            }
            for (int j = 0; j < campMines[0].length; j++) {
                System.out.print(campMines[i][j] + "  ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

    }

    static boolean contaMines(String[][] camp, String[][] campMines, int f, int c, int cont, boolean partidaencurs, int mines) {

        if (campMines[f][c].equals("■")) {

            if (c + 1 < campMines[f].length && campMines[f][c + 1].equals("✶")) {
                cont++;
            }

            if (c - 1 >= 0 && campMines[f][c - 1].equals("✶")) {
                cont++;
            }

            if (f - 1 >= 0 && campMines[f - 1][c].equals("✶")) {
                cont++;
            }

            if (f + 1 < campMines.length && campMines[f + 1][c].equals("✶")) {
                cont++;
            }

            if (f - 1 >= 0) {
                if (c + 1 <campMines[f].length &&campMines[f - 1][c + 1].equals("✶")) {
                    cont++;
                }
            }

            if (f + 1 < campMines.length) {
                if (c + 1 < campMines[f].length && campMines[f + 1][c + 1].equals("✶")) {
                    cont++;
                }
            }

            if (f - 1 >= 0) {
                if (c - 1 >= 0 && campMines[f - 1][c - 1].equals("✶")) {
                    cont++;
                }
            }

            if (f + 1 < campMines.length) {
                if (c - 1 >= 0 && campMines[f + 1][c - 1].equals("✶")) {
                    cont++;
                }
            }

            String cont2 = Integer.toString(cont);

            camp[f][c] = cont2;

        } else {

            camp[f][c] = "✶";

            mostrarCamp(camp);

            System.out.print("Has explotat!!\n");

            mostrarCampMines(campMines);

            partidaencurs = false;

        }

        return partidaencurs;

    }

    static void veureRankings() {



    }

}